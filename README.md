# Samsung Galaxy J3 2016 SM-J320FN avec Android 7.1.x vers Lineage 14.1

## Introduction

Ce tuto explique comment installer Lineage sur un Samnsung Galaxy J3 2016 avec Linux.

C'est encore un work in progress, il est amené à être étoffé prochainement, mais la partie installation proprement dite est complète.

Le téléphone doit être chargé et/ou branché.

L'architecture est ARM (et non ARM64)

## Téléchargements :

- [\[Official\] Download LineageOS Samsung Galaxy J3 (2016) (SM-J320FN) Nougat 7.1.1 ](https://www.lineageosrom.com/2017/07/Download-LineageOS-Samsung-Galaxy-J3.html)
- [\[ROM\] LineageOS 14.1 for Samsung Galaxy J3 (2016) \[SM-J320FN/F/G/M\] \[UNOFFICIAL\]](https://forum.xda-developers.com/galaxy-j3-2016/development/rom-lineageos-14-1-samsung-galaxy-j3-t3667015)
- [Lineage](https://drive.google.com/file/d/1KYhrg04jhpHC3OpbK6txvcdhZjpkHe3a/view)
- [TWRP](https://drive.google.com/file/d/0B35NGYJEHn3cSjJtYzZ0ZVdJSGs/view)
- [AddonSU](https://download.lineageos.org/extras)
- [AddonSU (direct download)](https://mirrorbits.lineageos.org/su/addonsu-14.1-arm-signed.zip)
- [OpenGAppstext](https://opengapps.org)

## Activer le mode développeur :

Paramètres &gt; A propos de l’appareil &gt; Infos logiciel &gt; taper 7
fois sur « numéro de version (ou de build) »

## Installer TWRP avec Linux :

On utilise heimdall en ligne de commande :
<https://doc.ubuntu-fr.org/heimdall>

- Placer le fichier .img à installer dans un répertoire contenant un chemin sans accents ni caractères spéciaux

- Installer le paquet adb

- Activer le mode de débogage USB et unlocker le bootloader en activant le débogage ADB (cet oubli est cause de messages d’erreurs dans heimdall)

- Rallumer le téléphone en appuyant sur volume bas, bouton menu, bouton power pour activer le mode download. Appuyer sur vol haut à apparition du menu pour continuer.

- Entrer la commande suivante dans heimdall (attention à la casse) :

```
heimdall flash --RECOVERY twrp.img
```

- Attendre la fin du chargement et l’indication que l’opération s’est achevée avec succès.:-)

- Attention ! Le chargement est rapide et le téléphone redémarre aussitôt. Il faut donc appuyer sur volume haut-menu-power pour qu’il redémarre sur TWRP, sinon tout est à recommencer.

## Effectuer une sauvegarde du système Android avec TWRP :

- Dans le menu TWRP, entrer dans le menu « Sauvegarder »

- Sélectionner les éléments à sauvegarder (minimum Amorçage, Système et Data) ainsi éventuellement que le nom du fichier et sa destination.

- Faire glisser en bas pour confirmer

- Attendre que le processus se termine avec succès

Installer Lineage avec TWRP :

- Placer le fichier à installer à la racine de la carte SD

- Dans le menu principal, cliquer sur « Installer », choisir le zip à installer et go !

- Attendre que le processus se termine avec succès

- Vider le Cache et Dalvik comme proposé à la fin ou via l’outil « Formater » avant de redémarrer (cela peut prendre quelques minutes)

- Si le système est étendu sur une partie de la carte SD, la sauvegarde va se diviser entre la carte Sd et l'extension. Mieux vaut sauvegarder sur une clé USB OTG.

## Rooter Lineage :

L’installation de l’addon peut se faire en même temps que l’installation
de Lineage.

- Suivre les étapes d’installation de Lineage avec TWRP en utilisant à la place le zip de l’addon

- Dans Lineage, activer le mode développeur comme vu au début

- Le rootage est disponible dans les outils pour développeur.

## Options pour développeurs

### Activation


* Accéder aux paramètres de l’appareil Android
* Aller dans les paramètres système
* Aller dans « À propos du téléphone »
* Tapoter 7 fois « numéro de build »
* Les options pour les développeurs apparaissent dans les paramètres système

### Désactivation

* Se rendre dans la section Applications des paramètres système
* Sélectionner l’application Paramètres, ou alors de faire un appui long sur le raccourci dans le tiroir d’applications.
* Cliquer sur Stockage
* Effacer les données et Vider le cache.

## Open GApps

### Intérêt

Open GApps est un projet libre qui permet d'installer une surcouche Google plus ou moins complète dans Lineage afin de pouvoir utiliser les applications le nécessitant.

### Installation

La version Pico, de part sa légèreté, est idéale. La version Stock est celle proposée par défaut et il est en principe inutile de prendre plus lourd.

### Désinstallation

La désinstallation n'est pas si simple. Mieux vaut sauvegarder son système et/ou ses données avant de procéder. Voici les étapes à suivre :

- Booter sur TWRP ;
- Effacer (reformater) la partition système, puis redémarrer denouveau sur TWRP ;
- Dans le shell ADB ou le shell TWRP, taper la ligne de commande suivante :
```
find /data -iname '*google*' -exec rm -rf '{}' \;
```
Ceci va effacer tous les fichiers estampillés "Google" dans le dossier `/data` du téléphoen.
- Flasher le système ou restaurer l'image précédemment sauvegardée.
- Vider le cache et rebooter.

### Liens

- https://opengapps.org/
- https://github.com/opengapps/opengapps/issues/551

## Play Protect

### Intérêt

Activer Play Protect peut permettre d'avoir accès aux achats via Google Pay, notamment dans le Play Store. Il est aussi nécessaire pour utiliser l'ensemble des fonctionnalités de Lydia, par exemple. L'installation d'OpenGApps est nécessaire.

### Activation

Pour ce faire, il faut obtenir son identifiant Google Service Framework (GSF) on peut utiliser soit ADB (voir lien Google ci-après), soit une application dédiée comme Device ID (disponible via le Play Store). Voici les commandes ADB à utiliser :

```
$ adb root
$ adb shell 'sqlite3 /data/data/com.google.android.gsf/databases/gservices.db \
    "select * from main where name = "android_id";"'
```

Ensuite, il faut se connecter au compte Google utilisé avec le téléphone et se rendre sur [la page dédiée du site de Google](https://www.google.com/android/uncertified/) pour activer la fonctionnalité en y enregistrant l'identifiant GSF. L'activation peut prendre jusqu'à 48 heures. Il peut être nécessaire d'effacer les données du Play Store et de redémarrer l'appareil pour que le changement prenne effet.

### Désactivation des fonctionnalités de scanner d'applications

Enfin, on peut désactiver l'analyse des applications et l'amélioration de la détection des applis dangereuses dans les paramètres Play Protect accessible via le Play Store. C'est utile si on veut éviter d'envoyer à Google des données sur les applications installées et si on installe régulièrement des applications en passant par des stores alternatifs et qu'on souhaite éviter d'avoir un avertissement de Play Protect à chaque installation.

### Désactivation

Effactuer un factory reset ou désinstaller les Open GApps désactivera complètement Play Protect en réinitialisant le GSF.

### Liens

- https://www.01net.com/astuces/comment-certifier-son-smartphone-android-pour-ne-pas-se-faire-bloquer-par-google-1412677.html
- https://www.google.com/android/uncertified/
- https://support.mobile-tracker-free.com/hc/fr/articles/360005346953-Comment-d%C3%A9sactiver-Google-Play-Protect-
- https://lineageos.org/Google-Play-Certification/

## Utiliser l'outil ADP sous Linux Ubuntu

### installation

`# apt install adp`

Activer le débogage USB via les outils pour développeur (désactiver et réactiver au besoin).

### utilisation

Pousser un fichier d'installation vers le stockage interne étendu :

`abd push lineage-14.1-20200125-UNOFFICIAL-j3xnlte.zip /sdcard/`

Installer une ROM depuis TWRP, après avoir activé le transfert par ADB (opération à refaire pour chaque transfert) :

`adb sideload lineage-14.1-20200125-UNOFFICIAL-j3xnlte.zip`

Uploader un fichier depuis le téléphone

`adb pull fichier destination`

## Crypter le téléphone

- Cela peut avoir l'inconvénient de rendre illisibles ses données depuis un autre support.

- Il ne faut pas être en root

- Attention au code pin Android

- Partition à formater pour enlever le cryptage : data.

## Etendre le système sur une partie de la carte SD

Quand on demande à Lineage de formater la carte SD pour y étendre le système, l'OS utilise toute la carte, peu importe qu'on l'ait partitionnée au préalable pour, par exemple conserver une partition pour les données. Ceci peut être contourné comme suit :

- Utiliser une carte SD d'un format inférieur à celle qu'on souhaite utiliser et y étendre le système.

- Avec `dd`, copier cette carte sur la carte de plus grande capacité.

- Formater l'espace restant au format FAT32 pour créer une partition de données accessible depuis un ordinateur.

- Attention, TWRP ne voit apparemment plus la partie "données" comme une carte externe une fois ceci fait.

- Via le options pour développeur, il peut être intéressant de forcer la disponibilité du stockage externe afin de pouvoir installer les apps dessus (certaines apps s'installent par défaut sur la mémoire interne et ne peuvent être migrées sans l'activation de cette option). Les principaux inconvénients sont la disparition des icônes des applications migrées du bureau après redémarrage , l'impossibilité de leur associer des widgets (voir apps de sauvegarde du bureau Android et/ou launchers alternatifs) et la perte des données d'application à chaque réinstallation.

- Pour accéder à la partie "expand" on peut par exemple utiliser l'application SManager.

## Magisk

Magisk permet de faire croire au SafetyNet de Google que le téléphone n'est pas rooté. Après installation, l'appli de ma banque qui ne fonctionnait pas avec AddonSu s'est remise à fonctionner.

### Installation

- Désactiver tous les mods ayant modifié ou étant susceptibles d'avoir modifié la partition système pour supprimer toute trace du root : Busybox, XposedFramework mais aussi Adway, AFWall ou NetGuard (les désactiver suffit, on pourra les réactiver par la suite)

- Installer Magisk via TWRP

- Installer l'application Magisk Manager une fois le téléphone redémarré.

- Mods intéressants : Riru Core, Magisk Manager Recovery Mode (mm), SSH For Magisk, F-Droid Priviledged Extension...

- EdXposed en revanche empêche le téléphone de redémarrer, c'est ennuyeux car ce mode est très utile, problème à creuser...

[Edit :] Après quelques jours d'utilisation, le système s'avère buggé (Wifi qui tente en vain de se connecter notamment). Retour à AddonSU

### Fonctionnement de Magisk Manager Recovery Mode (mm) :

Très utile pour désinstaller un module au cas où le téléphone ne peut plus démarrer après son installation.

- Une fois dans TWRP, se rendre dans le terminal et lancer la commande `sh /sdcard/mm` puis choisir l'entrée de menu voulue.

- Pour retirer un module, sélectionner `r`, taper le nom du module, entrée, et `q` pour quitter. Redémarrer l'appareil.

### Liens

- [Download Magisk](https://forum.xda-developers.com/apps/magisk/official-magisk-v7-universal-systemless-t3473445)
- [How to install Magisk](https://www.xda-developers.com/how-to-install-magisk/)
- [Official Magisk Forum](https://forum.xda-developers.com/apps/magisk)
- [How to Switch from SuperSU to Magisk & Pass SafetyNet ](https://android.gadgethacks.com/how-to/magisk-101-switch-from-supersu-magisk-pass-safetynet-0177578/)
- [Magisk vs SuperSU](https://www.xda-developers.com/magisk-vs-supersu/)
- [SU Addon or Magisk?](https://forum.xda-developers.com/lineage/help/su-addon-magisk-t3944380)
- [Installer Magisk et Xposed sur lineage os 14.1 officielle nougat 7.1.2](https://www.phonandroid.com/forum/threads/installer-magisk-et-xposed-sur-lineage-os-14-1-officielle-nougat-7-1-2.198400/)
- [\[2019.5.1\]\[Magisk\] Systemless Xposed v89.3/v90.2-beta3 (SDK 21-27)](https://forum.xda-developers.com/xposed/unofficial-systemless-xposed-t3388268)

## Remarques post-installation :

- Si F-Droid n’affiche rien, il faut le mettre à jour. (patienter un peu)

- Régler la luminosité dans la barre des tâches : Paramètres &gt; Barre d’état &gt; Curseur de luminosité

- Dans le menu "Mémoire" on peut régler les applis qui se lancent au démarrage.

**Beaucoup de choses dans les options pour développeurs, dont :**

- Sélectionner une configuration USB : en charge pour charger, MTP pour transférer des fichiers sur un ordi

- Le menu application permet de voir et configurer le détail des autorisations des applis et de définir les applis par défaut (menu en haut à droite)

- Attention avec la protection des applis : cela les fait disparaître du menu principal (pour accéder aux paramètres si protégés : appui long sur l’écran d’accueil &gt; paramètres en bas à droite)

- Dans le menu batterie, clic sur les trois points en haut à droite pour accéder aux options d’optimisation.

**Nextcloud :**

- Installer OpenTasks avant DavDroid

- Installer Nextcloud

- Synchroniser contacts et calendrier via les paramètres de Nextcloud

- Le calendrier devient visible dans l’appli Agenda

**Appli téléphone :**

Il est possible de décrocher/raccrocher avec les boutons du téléphone
(rubrique « boutons ») des paramètres.

**Géolocalisation :**

Si pb, débloquer la protection des données de Firefox via notification ou via les options avancées (dispos en haut à droite) de la protection des données.

Partage de connexion :

Attention économiseur de données

Internet pas dispo sauf Whatsapp Facebook :

Attention à être en GPRS et pas en WAP

**Désactivation du cercle blanc qui apparaît à l'endroit où on clique**

Menu "Saisie" > Désactiver "Afficher éléments sélectionnés"

## Remarques TWRP

- Pour pouvoir écrire sur une clé USB depuis TWRP, il faut la formater dans TWRP.
